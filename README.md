This data repository accompanies the paper
[Benchmarks for single-phase flow in fractured porous media]
(https://doi.org/10.1016/j.advwatres.2017.10.036)
by B. Flemisch, I. Berre, W. Boon, A. Fumagalli, N. Schwenck, A. Scotti,
I. Stefansson and A. Tatomir.

If you like to compare the result of your method with a benchmark reference
solution, please follow the instructions given in
[scripts/README.md](scripts/README.md).

Four benchmarks are considered in the following subfolders with numbers
referring to the numbering in the paper:

* [geiger](geiger): Benchmark 1, a regular fracture network.

* [hydrocoin](hydrocoin): Benchmark 2, a well established benchmark for groundwater flow
that contains two crossing, highly permeable fractures and a non-straight surface.

* [complex](complex): Benchmark 3, a small but complex fracture network exhibiting
    ending and intersecting fractures.

* [real](real): Benchmark 4, a case synthesized from a real application.

For each benchmark, grid and result data are provided in corresponding subfolders:

* `grids`: the employed computational grids, coming in the formats:

    - `.msh`, `.geo`: Gmsh output and input.
    - `.dgf`: Dune grid format.
    - `.GRDECL`: Petrel/Eclipse.
    - `.art`: FRAC3D/ART3D.

* `results`: result files of the following types:

    - `.vtk, .vtu`: VTK.
    - `.mat`: Matlab.
    - `.csv`: comma separated values for one-dimensional data.

Both grid and result files are structured according to the employed discretization method:

* `boxdfm`: vertex-centered, continuous-pressure, conforming lower-dimensional DFM (Box-DFM).
* `ccdfm`: cell-centered, discontinuous-pressure, conforming DFM (CC-DFM).
* `edfm`: continuous-pressure, non-conforming embedded DFM (EDFM).
* `mortardfm`: cell-centered, discontinuous-pressure, geometrically-conforming mortar DFM (mortar-DFM).
* `pxfem`: discontinuous-pressure, non-conforming primal XFEM (P-XFEM).
* `dxfem`: discontinuous-pressure, non-conforming dual XFEM (D-XFEM).
* `mfd`: reference Solutions calculated with mimetic finite differences (MFD).

Matlab scripts are provided for reproducing the numbers and figures from the
benchmark result sections.

In particular, the following scripts can be used for the 1d plots:

* Figure 11: [plot_conductive_y.m](geiger/results/plot_conductive_y.m) and
[plot_conductive_x.m](geiger/results/plot_conductive_x.m) in [geiger/results](geiger/results).
* Figure 14: [plot_blocking_diag.m](geiger/results/plot_blocking_diag.m) in [geiger/results](geiger/results).
* Figure 18: [plot_depth_200.m](hydrocoin/results/plot_depth_200.m) in [hydrocoin/results](hydrocoin/results).
* Figures 22 and 24: [plot_line.m](complex/results/plotLine/plot_line.m) in [complex/results/plotLine](complex/results/plotLine).
* Figure 29: [plot_depth_500.m](real/results/plot_depth_500.m) and [plot_x625.m](real/results/plot_x625.m) in
[real/results](real/results).
