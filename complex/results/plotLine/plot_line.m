close all;
clear all;
clc;
figure('Position', [0, 0, 666, 500]);

hold on;

% equi-dimensional MFD
mfd = csvread('REF_0p50p9.csv', 1, 0);
x=mfd(:,4);
y=mfd(:,5);
s=(x.^2+(y-.5).^2).^0.5;
plot(s, mfd(:, 1), 'Color', 'k', 'LineWidth', 1);

% Box-DFM
box = csvread('box_0p50p9.csv', 1, 0);
x=box(:,13);
y=box(:,14);
s=(x.^2+y.^2).^0.5;
s=box(:,15);
plot(s, box(:, 4), 'Color', 'g', 'LineWidth', 1);

% TPFA
tpfa = csvread('tpfa_tb.csv', 1, 0);
x=tpfa(:,4);
y=tpfa(:,5);
s=tpfa(:,3);
plot(s, tpfa(:, 1), 'Color', 'c', 'LineWidth', 1);

% TPFA*
tpfastar = csvread('tpfa_ic_tb.csv', 1, 0);
x=tpfastar(:,4);
y=tpfastar(:,5);
s=tpfastar(:,3);
plot(s, tpfastar(:, 1), ':c', 'LineWidth', 3);
% MPFA
mpfa = csvread('mpfa_tb.csv', 1, 0);
x=mpfa(:,4);
y=mpfa(:,5);
s=mpfa(:,3);
plot(s, mpfa(:, 1), 'Color', [0 .5 0], 'LineWidth', 1);

% EDFM
edfm= csvread('edfm_0p50p9.csv', 1, 0);
x=edfm(:,2);
y=edfm(:,3);
s=(x.^2+(y-.5).^2).^0.5;
plot(s, edfm(:, 1), 'Color', 'r', 'LineWidth', 1);

% Mortar-DFM
mortar= csvread('mortar_0p50p9.csv', 1, 0);
x=mortar(:,4);
y=mortar(:,5);
s=(x.^2+(y-.5).^2).^0.5;
plot(s, mortar(:, 1),  'Color', [1 0.7 0], 'LineWidth', 1);

% P-XFEM
%p_xfem = csvread('permeable_bernd_xfem_y_eq_0.7.csv', 1, 0);
%plot(p_xfem(:, 3), p_xfem(:, 1), 'Color', 'b', 'LineWidth', 1);

% D-XFEM
dxfem= csvread('DXFEM_0p50p9.csv', 1, 0);
x=dxfem(:,2);
y=dxfem(:,3);
s=((x).^2+(y-.5).^2).^0.5;
plot(s, dxfem(:, 1),'Color', 'm', 'LineWidth', 1);
grid on
% format the plot
ylim([1.5  4]);
xlabel('arc length', 'FontSize', 12, 'FontWeight', 'bold', 'Color', 'k');
ylabel('pressure', 'FontSize', 12, 'FontWeight', 'bold', 'Color', 'k');
set(gca, 'FontSize', 12);
legend('Reference', 'Box', 'TPFA', 'TPFA*', 'MPFA', 'EDFM', 'Flux-Mortar', 'D-XFEM','location','southeast');

set(gcf, 'PaperPositionMode', 'auto')
fig_pos = get(gcf, 'PaperPosition');
set(gcf, 'PaperSize', [fig_pos(3) fig_pos(4)+1])
saveas(gcf, 'complextopbottom_overline', 'pdf')
%%

close all;
clear all;
clc;
figure('Position', [0, 0, 666, 500]);
hold on

% equi-dimensional MFD
mfd = csvread('REF_0p50p9LR.csv', 1, 0);
x=mfd(:,4);
y=mfd(:,5);
s=(x.^2+(y-.5).^2).^0.5;
plot(s, mfd(:, 1), 'Color', 'k', 'LineWidth', 1);

% Box-DFM
box = csvread('box_0p50p9LR.csv', 1, 0);
x=box(:,14);
y=box(:,15);
s=(x.^2+(y-.5).^2).^0.5;
s=box(:,15);
plot(s, box(:, 4), 'Color', 'g', 'LineWidth', 1);

% TPFA
mfd = csvread('tpfa_lr.csv', 1, 0);
x=mfd(:,4);
y=mfd(:,5);
s=(x.^2+(y-.5).^2).^0.5;
plot(s, mfd(:, 1), 'Color', 'c', 'LineWidth', 1);

% TPFA*
mfd = csvread('tpfa_ic_lr.csv', 1, 0);
x=mfd(:,4);
y=mfd(:,5);
s=(x.^2+(y-.5).^2).^0.5;
plot(s, mfd(:, 1), ':c', 'LineWidth', 3);
% MPFA
mfd = csvread('mpfa_lr.csv', 1, 0);
x=mfd(:,4);
y=mfd(:,5);
s=(x.^2+(y-.5).^2).^0.5;
plot(s, mfd(:, 1), 'Color', [0 .5 0], 'LineWidth', 1);
% EDFM
mfd= csvread('edfm_0p50p9LR.csv', 1, 0);
x=mfd(:,2);
y=mfd(:,3);
s=(x.^2+(y-.5).^2).^0.5;
plot(s, mfd(:, 1), 'Color', 'r', 'LineWidth', 1);

% Mortar-DFM
mfd= csvread('mortar_0p50p9LR.csv', 1, 0);
x=mfd(:,4);
y=mfd(:,5);
s=(x.^2+(y-.5).^2).^0.5;
plot(s, mfd(:, 1),  'Color', [1 0.7 0], 'LineWidth', 1);

% P-XFEM
%p_xfem = csvread('permeable_bernd_xfem_y_eq_0.7.csv', 1, 0);
%plot(p_xfem(:, 3), p_xfem(:, 1), 'Color', 'b', 'LineWidth', 1);

% D-XFEM
mfd= csvread('DXFEM_0p50p9LR.csv', 1, 0);
x=mfd(:,2);
y=mfd(:,3);
s=(x.^2+(y-.5).^2).^0.5;
plot(s, mfd(:, 1),'Color', 'm', 'LineWidth', 1);
grid on

% format the plot
ylim([0.8  4.2]);
xlabel('arc length', 'FontSize', 12, 'FontWeight', 'bold', 'Color', 'k');
ylabel('pressure', 'FontSize', 12, 'FontWeight', 'bold', 'Color', 'k');
set(gca, 'FontSize', 12);
legend('Reference', 'Box', 'TPFA', 'TPFA*', 'MPFA', 'EDFM', 'Flux-Mortar', 'D-XFEM','location','northeast');

set(gcf, 'PaperPositionMode', 'auto')
fig_pos = get(gcf, 'PaperPosition');
set(gcf, 'PaperSize', [fig_pos(3) fig_pos(4)+1])
saveas(gcf, 'complexleftright_overline', 'pdf')
