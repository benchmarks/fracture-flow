close all;
clear all;
clc;
figure('Position', [0, 0, 666, 500]);
hold on;

% equi-dimensional MFD
mfd = csvread('mfd/mfd_hydrocoin_200.csv', 1, 0);
mfd(:, 4) = mfd(:, 4)/1000.0;
plot(mfd(:, 4), mfd(:, 1), 'Color', 'k', 'LineWidth', 1);

% Box-DFM
box_dfm = csvread('boxdfm/boxdfm_hydrocoin_200.csv', 1, 0);
box_dfm(:, 15) = box_dfm(:, 15)/1000;
plot(box_dfm(:, 15), box_dfm(:, 4), 'Color', 'g', 'LineWidth', 1, 'Linestyle', '-');

% CC-DFM
ccdfm = csvread('ccdfm/mpfa_hydrocoin_200.csv', 1, 0);
ccdfm(:, 4) = ccdfm(:, 4)/1000.0;
plot(ccdfm(:, 4), ccdfm(:, 1), 'Linestyle', '-', 'Color', 'c', 'LineWidth', 1);

% MPFA
ccdfm = csvread('ccdfm/ccdfm_hydrocoin_200.csv', 1, 0);
ccdfm(:, 4) = ccdfm(:, 4)/1000.0;
plot(ccdfm(:, 4), ccdfm(:, 1), 'Linestyle', '-', 'Color', [0 .5 0], 'LineWidth', 1);

% EDFM
edfm = csvread('edfm/edfm_hydrocoin_200.csv', 1, 0);
edfm(:, 4) = edfm(:, 4)/1000.0;
plot(edfm(:, 4), edfm(:, 1), 'Linestyle', '-', 'Color', 'r', 'LineWidth', 1);

% Mortar-DFM
mortar = csvread('mortardfm/mortardfm_hydrocoin_200.csv', 1, 0);
mortar(:, 4) = mortar(:, 4)/1000.0;
plot(mortar(:, 4), mortar(:, 1), 'Linestyle', '-', 'Color', [1 0.7 0], 'LineWidth', 1);

% P-XFEM
p_xfem = csvread('pxfem/pxfem_hydrocoin_200.csv', 1, 0);
p_xfem(:, 3) = p_xfem(:, 3)/1000;
plot(p_xfem(:, 3), p_xfem(:, 1), 'Color', 'b', 'LineWidth', 1, 'Linestyle', '-');

% D-XFEM
d_xfem_fine = csvread('dxfem/dxfem_hydrocoin_200.csv', 1, 0);
d_xfem_fine(:, 3) = d_xfem_fine(:, 3)/1000.0;
plot(d_xfem_fine(:, 3), d_xfem_fine(:, 1), 'Color', 'm', 'LineWidth', 1);

% format the plot
xlabel('arc length [km]', 'FontSize', 12, 'FontWeight', 'bold', 'Color', 'k');
ylabel('piezometric head [m]', 'FontSize', 12, 'FontWeight', 'bold', 'Color', 'k');
set(gca, 'FontSize', 12);
xlim([0 1.6]);
legend('Reference', 'Box', 'TPFA', 'MPFA', 'EDFM', 'Flux-Mortar', 'P-XFEM', 'D-XFEM', 'location', 'southwest');%'northeastoutside');
%figure('Position', [100, 100, 400, 300]);
set(gcf, 'PaperPositionMode', 'auto')
fig_pos = get(gcf, 'PaperPosition');
set(gcf, 'PaperSize', [fig_pos(3) fig_pos(4)+1])
saveas(gcf, 'hydrocoin_200', 'pdf')
