// Gmsh geometry specification for the hydrocoin example
// hybrid-dimensional

lc = 5e1;
lcf = 3.7e1;

Point(1) = {0, 150, 0, lc};
Point(2) = {400, 100, 0, lcf} ;
Point(3) = {800, 150, 0, lc} ;
Point(4) = {1200, 100, 0, lcf} ;
Point(5) = {1600, 150, 0, lc} ;
Point(6) = {1600, -1000, 0, lc} ;
Point(7) = {1500, -1000, 0, lcf} ;
Point(8) = {1000, -1000, 0, lcf} ;
Point(9) = {0, -1000, 0, lc} ;
Point(10) = {1076.92307692308, -576.923076923077, 0, lcf} ;

// boundary
Line(1) = {1, 2} ;
Line(2) = {2, 3} ;
Line(3) = {3, 4} ;
Line(4) = {4, 5} ;
Line(5) = {5, 6} ;
Line(6) = {6, 7} ;
Line(7) = {7, 8} ;
Line(8) = {8, 9} ;
Line(9) = {9, 1} ;

// left fracture
Line(10) = {2, 10} ;
Line(11) = {10, 7} ;

// right fracture
Line(12) = {8, 10} ;
Line(13) = {10, 4} ;

Line Loop(1) = {1, 10, -12, 8, 9} ;
Line Loop(2) = {2, 3, -13, -10} ;
Line Loop(3) = {7, 12, 11} ;
Line Loop(4) = {4, 5, 6, -11, 13} ;

Plane Surface(1) = {1} ;
Plane Surface(2) = {2} ;
Plane Surface(3) = {3} ;
Plane Surface(4) = {4} ;

Physical Line(0) = {1, 2, 3, 4, 5, 6, 7, 8, 9};
Physical Line(1) = {10, 11, 12, 13};
Physical Surface(0) = {1, 2, 3, 4};
