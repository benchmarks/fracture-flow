// Gmsh project created on Wed Apr 05 16:32:39 2017
Mesh.Algorithm = 6;

T = 1.0;

L = 105*T;
S = 0.38*L;
M = L*0.5;
XS= S;

Point(1) = {0, 150*T, 0, S};

Point(3) = {400*T, 100*T, 0, XS};
Point(5) = {800*T, 150*T, 0, S};
Point(6) = {1200*T, 100*T, 0, XS};
Point(9) = {1600*T, 150*T, 0, S};
Point(10) = {1600*T, -1000*T, 0, 1.1*L};
Point(11) = {1500*T, -1000*T, 0, L};
Point(13) = {1000*T, -1000*T, 0, L};
Point(15) = {0, -1000*T, 0, 1.1*L};
Point(16) = {1076.92*T, -576.92*T, 0, M};
Point(20) = {0, -400*T, 0, L};
Point(21) = {1600*T, -400*T, 0, L};


Line(11) = {15, 13};
Line(21) = {13, 16};
Line(31) = {16, 3};
Line(41) = {3, 1};
Line(61) = {1, 20};
Line(611)= {20, 15};


Line(72) = {13, 11};
Line(82) = {11, 16};

Line(93) = {11, 10};
Line(103) ={10, 21};
Line(1013)={21, 9};
Line(113) = {9, 6};

Line(133) = {6, 16};


Line(144) = {6, 5};
Line(154) = {5, 3};

Line Loop(1) = {11, 21, 31, 41, 61, 611};
Line Loop(2) = {72, 82, -21};
Line Loop(3) = {-82, 93, 103, 1013, 113, 133};
Line Loop(4) = {-31, -133, 144, 154};

Plane Surface(11) = {1};
Plane Surface(12) = {2};
Plane Surface(13) = {3};
Plane Surface(14) = {4};
