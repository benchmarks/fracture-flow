// Gmsh geometry specification for the hydrocoin example
// equi-dimensional

lc = 5.4321;
lcfleft = 1;
lcfright = 2;

Point(1) = {0, 150, 0, lc};
Point(2) = {394.285714286, 100.714285714,  0, lcfleft} ;
Point(3) = {400, 100, 0, lcfleft} ;
Point(4) = {404.444444444, 100.555555556, 0, lcfleft} ;
Point(5) = {800, 150, 0, lc} ;
Point(6) = {1192.66666667, 100.916666667, 0, lcfright} ;
Point(7) = {1200, 100, 0, lcfright} ;
Point(8) = {1207.6744186, 100.959302326, 0, lcfright} ;
Point(9) = {1600, 150, 0, lc} ;
Point(10) = {1600, -1000, 0, lc} ;
Point(11) = {1505, -1000, 0, lcfleft} ;
Point(12) = {1495, -1000, 0, lcfleft} ;
Point(13) = {1007.5, -1000, 0, lcfright} ;
Point(14) = {992.5, -1000, 0, lcfright} ;
Point(15) = {0, -1000, 0, lc} ;
Point(16) = {1071.34615385, -566.346153846, 0, lcfleft} ;
Point(17) = {1084.03846154, -579.038461538, 0, lcfleft} ;
Point(18) = {1082.5, -587.5, 0, lcfleft} ;
Point(19) = {1069.80769231, -574.807692308, 0, lcfleft} ;

Line(1) = {1,2} ;
Line(2) = {2,3} ;
Line(3) = {3,4} ;
Line(4) = {4,5} ;
Line(5) = {5,6} ;
Line(6) = {6,7} ;
Line(7) = {7,8} ;
Line(8) = {8,9} ;
Line(9) = {9,10} ;
Line(10) = {10,11} ;
Line(11) = {11,12} ;
Line(12) = {12,13} ;
Line(13) = {13,14} ;
Line(14) = {14,15} ;
Line(15) = {15,1} ;
Line(16) = {2,19} ;
Line(17) = {19,14} ;
Line(18) = {6,16} ;
Line(19) = {16,4} ;
Line(20) = {13,18} ;
Line(21) = {18,12} ;
Line(22) = {11,17} ;
Line(23) = {17,8} ;
Line(24) = {16,17} ;
Line(25) = {17,18} ;
Line(26) = {18,19} ;
Line(27) = {19,16} ;

Line Loop(1) = {1,16,17,14,15} ;
Line Loop(2) = {4,5,18,19} ;
Line Loop(3) = {8,9,10,22,23} ;
Line Loop(4) = {12,20,21} ;
Line Loop(5) = {2,3,-19,-27,-16} ;
Line Loop(6) = {6,7,-23,-24,-18} ;
Line Loop(7) = {11,-21,-25,-22} ;
Line Loop(8) = {13,-17,-26,-20} ;
Line Loop(9) = {24,25,26,27} ;

Plane Surface(1) = {1} ;
Plane Surface(2) = {2} ;
Plane Surface(3) = {3} ;
Plane Surface(4) = {4} ;
Plane Surface(5) = {5} ;
Plane Surface(6) = {6} ;
Plane Surface(7) = {7} ;
Plane Surface(8) = {8} ;
Plane Surface(9) = {9} ;

Mesh.Algorithm = 8;
Recombine Surface{1,2,3,4,5,6,7,8,9};
