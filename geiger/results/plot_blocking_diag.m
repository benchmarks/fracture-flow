close all;
clear all;
clc;
figure('Position', [0, 0, 666, 500]);

hold on;

% equi-dimensional MFD
mfd = csvread('mfd/mfd_geiger_blocking_0_0.1_to_0.9_1.csv', 1, 0);
plot(mfd(:, 6), mfd(:, 1), 'Color', 'k', 'LineWidth', 1);

% Box-DFM
dfm = csvread('boxdfm/boxdfm_geiger_blocking_0_0.1_to_0.9_1.csv', 1, 0);
plot(dfm(:, 15), dfm(:, 4), 'Color', 'g', 'LineWidth', 1);

% CC-DFM
ccdfm = csvread('ccdfm/ccdfm_geiger_blocking_0_0.1_to_0.9_1.csv', 1, 0);
plot( ccdfm(:, 3), ccdfm(:, 1), 'Color', 'c', 'LineWidth', 1);

% MPFA
mpfa = csvread('ccdfm/mpfa_geiger_blocking_0_0.1_to_0.9_1.csv', 1, 0);
plot( mpfa(:, 3), mpfa(:, 1), 'Color', [0 .6 0.6], 'LineWidth', 1);


% EDFM
edfm = csvread('edfm/edfm_geiger_blocking_0_0.1_to_0.9_1.csv', 1, 0);
plot( edfm(:, 3), edfm(:, 1), 'Color', 'r', 'LineWidth', 1);

% Mortar-DFM
mortar = csvread('mortardfm/mortardfm_geiger_blocking_0_0.1_to_0.9_1.csv', 1, 0);
plot( mortar(:, 3), mortar(:, 1), 'Color', [1 0.7 0], 'LineWidth', 1);

% P-XFEM
p_xfem = csvread('pxfem/pxfem_geiger_blocking_0_0.1_to_0.9_1.csv', 1, 0);
plot(p_xfem(:, 3), p_xfem(:, 1), 'Color', 'b', 'LineWidth', 1);

% D-XFEM
d_xfem_fine = csvread('dxfem/dxfem_geiger_blocking_0_0.1_to_0.9_1.csv', 1, 0);
plot(d_xfem_fine(:, 3), d_xfem_fine(:, 1), 'Color', 'm', 'LineWidth', 1);

% format the plot
xlabel('arc length', 'FontSize', 12, 'FontWeight', 'bold', 'Color', 'k');
ylabel('pressure', 'FontSize', 12, 'FontWeight', 'bold', 'Color', 'k');
xlim([0 1.2728]);
set(gca, 'FontSize', 12);
legend('Reference', 'Box', 'TPFA', 'MPFA', 'EDFM', 'Flux-Mortar', 'P-XFEM', 'D-XFEM', 'location', 'northeast');

set(gcf, 'PaperPositionMode', 'auto')
fig_pos = get(gcf, 'PaperPosition');
set(gcf, 'PaperSize', [fig_pos(3) fig_pos(4)+1])
saveas(gcf, 'geiger_blocking_diag', 'pdf')
