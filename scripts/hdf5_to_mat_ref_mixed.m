function hdf5_to_mat_ref_mixed(infilename, outfilename, domainarea)
% hdf5_to_mat_ref_mixed Convert a reference solution from HDF5 to Matlab format.
%
% Signature:
% hdf5_to_mat_ref_mixed(infilename, outfilename, domainarea)
%
% Parameters:
% infilename: the name of the HDF5 file containing the matrix solution
% outfilename: the name of the output Matlab .mat-file
% domainarea: area of the computational domain

x = h5read(infilename, '/Block_0_t000000/Geometry/Coordinates');
x = x([1 2], :)';
pmixed = h5read(infilename, strcat('/Block_0_t000000/Cell/', 'pressure'));
p = [];
kmixed = h5read(infilename, strcat('/Block_0_t000000/Cell/', 'permeability'));
k = [];
tmixed = h5read(infilename, '/Block_0_t000000/Topology/DataArray0');
mixedIdx = 1;
eIdx = 1;
t = [];
while (mixedIdx < length(tmixed))
    eType = tmixed(mixedIdx);
    if eType == 4
        t(eIdx, 1:3) = tmixed(mixedIdx+1:mixedIdx+3)';
        p(eIdx) = pmixed(mixedIdx);
        k(eIdx) = kmixed(mixedIdx);
        mixedIdx = mixedIdx + 4;
        eIdx = eIdx + 1;
    else
        t(eIdx, 1:3) = tmixed([mixedIdx+1, mixedIdx+2, mixedIdx+4])';
        t(eIdx+1, 1:3) = tmixed([mixedIdx+1, mixedIdx+4, mixedIdx+3])';
        p(eIdx) = pmixed(mixedIdx);
        p(eIdx+1) = pmixed(mixedIdx);
        k(eIdx) = kmixed(mixedIdx);
        k(eIdx+1) = kmixed(mixedIdx);
        mixedIdx = mixedIdx + 5;
        eIdx = eIdx + 2;
    end
end
t = t + 1;

save(outfilename, 't', 'p', 'x', 'k', 'domainarea');
fprintf('Wrote file %s.\n', outfilename);

return
