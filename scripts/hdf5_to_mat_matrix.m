function hdf5_to_mat_matrix(infilename, cellwise, solutionname, outfilename)
% hdf5_to_mat_matrix Convert a matrix solution from HDF5 to Matlab format.
%
% Signature:
% hdf5_to_mat_matrix(infilename, solutionname, outfilename)
%
% Parameters:
% infilename: the name of the HDF5 file containing the matrix solution
% cellwise: true for a cell-wise solution vector, false for a vertex-wise
% solutionname: the name of the solution vector inside the HDF5 file
% outfilename: the name of the output Matlab .mat-file

T = h5read(infilename, '/Block_0_t000000/Topology');
T = T' + 1;
Points = h5read(infilename, '/Block_0_t000000/Geometry/Coordinates');
Points = Points([1 2], :)';
if cellwise
  P = h5read(infilename, strcat('/Block_0_t000000/Cell/', solutionname));
else
  pvertex = h5read(infilename, strcat('/Block_0_t000000/Node/', solutionname));
  P = zeros(size(T, 1), 1);
  factor = 1/size(T, 2);
  for i = 1:size(T, 2)
    P = P + factor.*pvertex(T(:, i));
  end
end

save(outfilename, 'T', 'P', 'Points');
fprintf('Wrote file %s.\n', outfilename);

return
