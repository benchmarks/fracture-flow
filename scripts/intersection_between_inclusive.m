function [ intersection ] = intersection_between_inclusive( k, kIsVertical, l, lIsVertical, between)
%intersection_between_inclusive Calculates intersections of two lines.
% The lines are  specified by two sets of points. If between is true, an empty value is returned if the intersection
%is outside the endpoints of either one of the lines
%   lines in format [x1, y1; x2, y2]
%                   [11, 12; 21, 22]
% line k = ax + b,      l = cx + d
% vk = true iff k vertical

if kIsVertical
    
    c = (l(2,2) - l(1,2)) / (l(2,1)-l(1,1));
    d = l(1,2) - c*l(1,1);
    y = c * k(1) + d;
    if isnan(y)
        if lIsVertical
            intersection = [];
        else
            error('non-parallel lines did not intersect');
        end
    elseif between && (y > max(k(3:4)) || y < min(k(3:4)) || ...
                       y > max(l(3:4)) || y < min(l(3:4)))
        intersection = [];
    else
        intersection = [k(1), y];
    end
    
elseif lIsVertical
    a = (k(2,2) - k(1,2)) / (k(2,1)-k(1,1));
    b = k(1,2) - a*k(1,1);
    x = l(1);
    y = a * x + b;

   if between && (y > max(l(3:4)) || y < min(l(3:4)) || ...
                  y > max(k(3:4)) || y < min(k(3:4)))
        intersection = [];
    else
        intersection = [x, y];
    end
else
    a = (k(2,2) - k(1,2)) / (k(2,1)-k(1,1));
    c = (l(2,2) - l(1,2)) / (l(2,1)-l(1,1));
    b = k(1,2) - a*k(1,1);
    d = l(1,2) - c*l(1,1);
    
    x = (d-b)/(a-c);
    
    if between && (x > max(l(1:2)) || x < min(l(1:2)) || ...
                   x > max(k(1:2)) || x < min(k(1:2)))
        intersection = [];
    else
        intersection = [x, a * x + b];
    end
end
end

