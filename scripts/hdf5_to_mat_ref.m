function hdf5_to_mat_ref(infilename, outfilename, domainarea)
% hdf5_to_mat_ref Convert a reference solution from HDF5 to Matlab format.
%
% Signature:
% hdf5_to_mat_ref(infilename, outfilename, domainarea)
%
% Parameters:
% infilename: the name of the HDF5 file containing the matrix solution
% outfilename: the name of the output Matlab .mat-file
% domainarea: area of the computational domain

tvec = h5read(infilename, '/Block_0_t000000/Topology');
tvec = tvec' + 1;
t = {};
for i = 1:length(tvec)
    t{i} = tvec(i, :);
end
t = t';
x = h5read(infilename, '/Block_0_t000000/Geometry/Coordinates');
x = double(x([1 2], :)');
p = h5read(infilename, strcat('/Block_0_t000000/Cell/', 'pressure'));
k = h5read(infilename, strcat('/Block_0_t000000/Cell/', 'permeability'));

save(outfilename, 't', 'p', 'x', 'k', 'domainarea');
fprintf('Wrote file %s.\n', outfilename);

return
