function [totalRelativeFractureError] = compute_1d_error( coarseFile, referenceFile, kmat )
%compute1dError Evaluates the norm in each of the fractures
% 
% The coarse file must contain a nFractures x 1 cell array with fracXAndP for
% each of the fractures, see the Description for details on how each
% fracXAndP should be constructed.
% The reference file should contain a triangulation t, point list x and
% solution vector p.

load(referenceFile,'p','t','x','k')
load(coarseFile,'fracXAndPCell')
maxp = max(p);
minp = min(p);
if nargin == 2
    kmat = 1;
end
matrixIndices = find(k == kmat);
t(matrixIndices) = [];
p(matrixIndices) = [];

nFractures                      = length(fracXAndPCell);
idividualRelativeFractureError  = zeros(nFractures,1); %Included for comparison
% between the fractures
fractureErrorSquared            = zeros(nFractures,1);
normalizationSquared            = zeros(nFractures,1);
frac = true;

for i = 1:nFractures
    fracXAndP                   = fracXAndPCell{i};
    [idividualRelativeFractureError(i),fractureErrorSquared(i),normalizationSquared(i)] = ...
                                  line_norm(fracXAndP,p,t,x,frac,maxp,minp);
end

totalRelativeFractureError = sqrt(sum(fractureErrorSquared)/sum(normalizationSquared));
end

