function [ northWest, on] = check_points( x, a,b , isVertical)
%check_points Determines which of a number of 2D points in x lie north (west)
% of the line ax + b and if any points lie on the line
%   If the line is vertical, the constant x-value goes in the input a

if isVertical
    northWest   = x(:,1) < a;
    on          = x(:,1) == a;
elseif a == 0
    northWest   = x(:,2) > b;
    on          = x(:,2) == b;
else 
    northWest   = a*x(:,1) + b < x(:,2);
    on          = a*x(:,1) + b == x(:,2);
end

end

