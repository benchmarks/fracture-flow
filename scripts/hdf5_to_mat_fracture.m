function hdf5_to_mat_fracture(basefilename, numfractures, solutionname, outfilename)
% hdf5_to_mat_fracture Convert a fracture network solution from HDF5 to Matlab format.
%
% Signature:
% hdf5_to_mat_fracture(basefilename, numfractures, solutionname, outfilename)
%
% Parameters:
% basefilename: the base name of the HDF5 files containing the fracture
%               network solution, the individual files for each fracture
%               branch are expected to be named 'basefilenameX.h5' where
%               'X' is the index of the fracture branch
% numfractures: the number of individual fracture branches (equal to the
%               number of input files)              
% solutionname: the name of the solution vector inside the HDF5 file
% outfilename: the name of the output Matlab .mat-file

fracXAndPCell = cell(numfractures, 1);
for fracidx = 1:numfractures
    filename = strcat(basefilename, num2str(fracidx), '.h5');
    fraccoords = h5read(filename, '/Block_0_t000000/Geometry/Points');
    fracpressure = h5read(filename, strcat('/Block_0_t000000/Node/', solutionname));
    fraccoords(:, isnan(fracpressure)) = [];
    fracpressure(isnan(fracpressure)) = [];
    numpoints = size(fraccoords, 2);
    Xp = zeros(2*(numpoints-1), 3);
    Xp(1:2:end-1, 1:2) = fraccoords(1:2, 1:end-1)';
    Xp(2:2:end, 1:2) = fraccoords(1:2, 2:end)';
    Xp(1:2:end-1, 3) = fracpressure(1:end-1);
    Xp(2:2:end, 3) = fracpressure(2:end)';
    fracXAndPCell{fracidx} = Xp;
end

save(outfilename, 'fracXAndPCell', '-append');
fprintf('Wrote file %s.\n', outfilename);

return
