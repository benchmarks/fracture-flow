function [ a ] = area_on_face( polygon,Polygon )
%area_on_face calculates intersection area between convex polygons
%   The small polygon pol is cut by one of the edges of the much larger
%   Pol. Their intersection area is computed.
NVertices   = size(Polygon,1);
nVertices   = size(polygon,1);
northWest   = false(nVertices,NVertices);
on          = false(nVertices,NVertices);
allOneSide  = false(NVertices,1);
Polygon2        = [Polygon;Polygon(1,:)];
polygon2        = [polygon;polygon(1,:)];
isVertical         = false(NVertices,1);

for i = 1:NVertices
    if Polygon2(i)==Polygon2(i+1)
        isVertical(i)   = true;
        a               = Polygon2(i);
        b               = 0;
    else
        a       = (Polygon2(i+1,2)-Polygon2(i,2))/(Polygon2(i+1)-Polygon2(i));
        b       = Polygon2(i,2)-a*Polygon2(i);
    end
    [northWest(:,i),on(:,i)]   = check_points(polygon,a,b,isVertical(i));
    allOneSide(i)       = all(northWest(:,i)) || all(~northWest(:,i));
end

% [nw(:,N),on(:,N)]   = check_points(pol,a,b,isv); % hva med på linje??
% alloneside(N)       = all(nw(:,N)) || all(~nw(:,N));


Faces           = find(~allOneSide);
NFaces          = length(Faces);
intersections   = zeros(NFaces*2,2);
j               = 0;
northWest2      = [northWest;northWest(1,:)];
on2             = [on;on(1,:)];

for k = 1:NFaces
    F = Faces(k);
    for i = 1:nVertices
        if on(i,F)
            if on2(i+1,F)
                sortedVertices              = sortrows([polygon2(i:i+1,:);Polygon2(F:F+1,:)]);
                intersections(j+1:j+2,:)    = sortedVertices(2:3,:);
                j                           = j+2;
            else
                j                           = j +1;
                intersections(j,:)          = polygon(i,:);
            end
        elseif on2(i+1,F)
            
        elseif northWest2(i,F) ~=northWest2(i+1,F)
            j   = j+1;
            ic2 = intersection_between_inclusive([Polygon2(F),Polygon2(F,2);Polygon2(F+1),Polygon2(F+1,2)], ...
                isVertical(F),[polygon2(i),polygon2(i,2);polygon2(i+1),polygon2(i+1,2)],polygon2(i)==polygon2(i+1),true);
            if ~isempty(ic2)
                intersections(j,:) = ic2;
            else 
                j = j - 1;
            end
        end
    end
end


inside          = inpolygon(polygon(:,1),polygon(:,2),Polygon(:,1),Polygon(:,2));
insidePoints    = polygon(inside,:);
try
    [~, a]          = convhull([intersections(1:j,:);insidePoints]);
catch
    % The exception should only occur if the intersection is empty. Displays the polygons
    % for the user to inspect.
    disp('Returned zero intersection area for coarse and fine polygons')
    disp(Polygon)
    disp(polygon)
    a = 0;
end
end