function [ intersectionPoints ] = intersections_of_cells( linePoints, ... 
                                  verticalLine,  cellPointsOn, cellPointsNortWestOn )
%UNTITLED5 The matrix intersectionPoints contains the intersections with the line
%specified by linePoints for each of the cells defined by the points in
%cellPointsOn. The format for the intersection points is x1,y1,x2,y2

intersectionPoints = zeros(length(cellPointsOn),4); 

for c = 1:length(cellPointsOn)
    ind = 1; % places the points in the right position in the matrix line (c,:)
    l = length(cellPointsOn{c});
    for v = 1:l
        if v < l
            i = v+1; % could perhaps be handled with modulus, but =0 has to 
            %be avoided
        else
            i = 1;
        end
        
        % If the two points of a face are on opposite sides of the line,
        % the face intersects the line:
        if cellPointsNortWestOn{c}(v) ~= cellPointsNortWestOn{c}(i) 
            facePoints = [cellPointsOn{c}(v,:); cellPointsOn{c}(i,:)];
            verticalFace = abs(cellPointsOn{c}(v,1)- cellPointsOn{c}(i,1)) < 5*eps;
            %find intersection points:
            intersectionPoints(c,ind:ind+1) = intersection(linePoints, ...
                                        verticalLine, facePoints, ...
                                        verticalFace);
                  
            ind = ind + 2;
        end
    end
   
end

                                    
                                   
end

