function [ intersection ] = intersection( k, vk, l, vl)
%UNTITLED2 Calculates intersections of two lines specified by two sets of
%points
%   lines in format [x1, y1; x2, y2]
%                   [11, 12; 21, 22]
% line k = ax + b,      l = cx + d
% vk = true iff k vertical
if vk
    c = (l(2,2) - l(1,2)) / (l(2,1)-l(1,1));
    d = l(1,2) - c*l(1,1);
    intersection = [k(1), c * k(1) + d];
elseif vl
    a = (k(2,2) - k(1,2)) / (k(2,1)-k(1,1));
    b = k(1,2) - a*k(1,1);
    intersection = [l(1), a * l(1) + b];
else
    a = (k(2,2) - k(1,2)) / (k(2,1)-k(1,1));
    c = (l(2,2) - l(1,2)) / (l(2,1)-l(1,1));
    b = k(1,2) - a*k(1,1);
    d = l(1,2) - c*l(1,1);
    
    x = (d-b)/(a-c);
    %y = a * x + b;
    intersection = [x, a * x + b];
end
end

