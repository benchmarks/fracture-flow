This folder contains several Matlab scripts which can be used to evaluate a solution to one of the benchmark cases with the corresponding reference solution.

### 1 Comparison of the solution in the matrix

#### 1.1 Preparation of the solution data

The matrix solution has to be provided in form of a Matlab `mat`-file which has to contain the following variables:

* `Points`: an `np x 2` matrix that contains the x and y coordinates of the `np` vertices of the computational grid in columns `1` and `2`, respectively. 
* `T`: an `nt x nvelem` element connectivity matrix which contains in each row an element's `nvelem` vertex indices with respect to the numbering induced by `Points`.
* `P`: an `nt`-size vector containing the element-wise solution values with respect to the numbering induced by `T`.

Is your original solution data available as a VTK file? A helper function `hdf5_to_mat_matrix.m` is provided which extracts the required `mat`-file from an HDF5-file, which in turn can be exported from Paraview: select `File` -> `Save Data...` -> `Files of type: Xdmf Data File`, use the produced `h5`-file. Type `help hdf5_to_mat_matrix` in Matlab for details.

#### 1.2 Performing the comparison

The function [compute_2d_error](compute_2d_error.m) can be used to calculate a relative L2-error of the matrix solution. It has the signature
```
function [relativeMatrixError] = compute_2d_error(coarseFile, referenceFile)
```
`coarseFile` is the `mat`-file as described in 1.1, `referenceFile` is one of the `mat`-files found in the `results/mfd`-subfolders, for example `../hydrocoin/results/mfd/mfd_hydrocoin.mat`.

### 2 Comparison of the solution in the fractures

#### 2.1 Preparation of the solution data

The fracture solution has to be provided in form of a Matlab `mat`-file which has to contain a Matlab cell array `fracXAndPCell`. Each cell `fracXAndPCell{i}` provides the solution on fracture branch `i` in form of a `(2.nc) x 3` matrix. As 2x3 block `fracXAndPCell{i}(2*e-1:2*e, :)` describes the solution on fracture element `e` in form of `[xStart, yStart, pStart; xEnd yEnd, pEnd]` where 
`xStart, yStart` / `xEnd, yEnd` indicate the two vertices of the fracture element and `pStart` / `pEnd` the corresponding solution values.

Is your original solution data available as a VTK file? A helper function `hdf5_to_mat_fracture.m` is provided which extracts the required `mat`-file from HDF5-files for the fracture branches, which in turn can be exported from Paraview: Perform a plot-over-line for a fracture branch, select `File` -> `Save Data...` -> `Files of type: Xdmf Data File`, use the produced `h5`-file. Type `help hdf5_to_mat_fracture` in Matlab for details.


#### 2.2 Performing the comparison

The function [compute_1d_error](compute_1d_error.m) can be used to calculate a relative L2-error of the fracture solution. It has the signature
```
function [totalRelativeFractureError] = compute_1d_error(coarseFile, referenceFile)
```
`coarseFile` is the `mat`-file as described in 2.1, `referenceFile` is one of the `mat`-files found in the `results/mfd`-subfolders, for example `../hydrocoin/results/mfd/mfd_hydrocoin.mat`.
