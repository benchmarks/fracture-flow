function [cellVerticesOn, verticesP] = combine_faces(cellVerticesOn, verticesP,isVertical)
%combine_faces calculates the average of the two pressure values for the faces
% which coincide with the line

if isVertical
    cellXOrY = sort([cellVerticesOn(:,2),cellVerticesOn(:,4),],2);
else
    cellXOrY = sort([cellVerticesOn(:,1),cellVerticesOn(:,3),],2);
end
for i = 1:length(verticesP)/2 % two values for each face
    ind = ismember(cellXOrY(:,1),cellXOrY(i,1));
    r = find(ind);
    n = length(ind);
    % remove the second cell
    cellVerticesOn = [cellVerticesOn(1:r(2)-1,:);cellVerticesOn(r(2)+1:n,:)];
    cellXOrY = [cellXOrY(1:r(2)-1,:); cellXOrY(r(2)+1:n, :)];
    % replace the first pressure by the mean of the two
    verticesP(r(1)) = mean(verticesP(ind));
    % and remove the second one
    verticesP = [verticesP(1:r(2)-1);verticesP(r(2)+1:n)];
        
end
end
