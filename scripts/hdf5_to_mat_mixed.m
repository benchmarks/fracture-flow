function hdf5_to_mat_mixed(infilename, cellwise, solutionname, outfilename)
% hdf5_to_mat_matrix Convert a matrix solution from HDF5 to Matlab format.
%
% Signature:
% hdf5_to_mat_matrix(infilename, solutionname, outfilename)
%
% Parameters:
% infilename: the name of the HDF5 file containing the matrix solution
% cellwise: true for a cell-wise solution vector, false for a vertex-wise
% solutionname: the name of the solution vector inside the HDF5 file
% outfilename: the name of the output Matlab .mat-file

Points = h5read(infilename, '/Block_0_t000000/Geometry/Coordinates');
Points = Points([1 2], :)';
if cellwise
  Pmixed = h5read(infilename, strcat('/Block_0_t000000/Cell/', solutionname));
  P = [];
end
Tmixed = h5read(infilename, '/Block_0_t000000/Topology/DataArray0');
mixedIdx = 1;
eIdx = 1;
T = [];
while (mixedIdx < length(Tmixed))
    eType = Tmixed(mixedIdx);
    if eType == 4
        T(eIdx, 1:3) = Tmixed(mixedIdx+1:mixedIdx+3)';
        if cellwise
            P(eIdx) = Pmixed(mixedIdx);
        end
        mixedIdx = mixedIdx + 4;
        eIdx = eIdx + 1;
    else
        T(eIdx, 1:3) = Tmixed([mixedIdx+1, mixedIdx+2, mixedIdx+4])';
        T(eIdx+1, 1:3) = Tmixed([mixedIdx+1, mixedIdx+4, mixedIdx+3])';
        if cellwise
            P(eIdx) = Pmixed(mixedIdx);
            P(eIdx+1) = Pmixed(mixedIdx);
        end
        mixedIdx = mixedIdx + 5;
        eIdx = eIdx + 2;
    end
end
T = T + 1;
if ~cellwise
  pvertex = h5read(infilename, strcat('/Block_0_t000000/Node/', solutionname));
  P = zeros(size(T, 1), 1);
  factor = 1/size(T, 2);
  for i = 1:size(T, 2)
    P = P + factor.*pvertex(T(:, i));
  end
end

save(outfilename, 'T', 'P', 'Points');
fprintf('Wrote file %s.\n', outfilename);

return
